import _ from 'lodash';
import objectAssign from 'object-assign';
import uuidv4 from 'uuid/v4';

var _list = [

  {
    id: uuidv4(),
    name: "Ana Menü",
    langId: 1,
    data: [
      {
        id: uuidv4(),
        name: "CEPTETEB NEDİR?",
        typeId: "1",
        isNewPage: false,
        data: {
          "menuType1": {
            pageId: uuidv4()
          }
        }
      }, {
        id: uuidv4(),
        name: "CEPTETEB'DE BANKACILIK",
        typeId: "2",
        isNewPage: false,
        data: {
          "menuType2": {
            url: "#"
          }

        },
        children: [
          {
            id: uuidv4(),
            name: "NASIL CEPTETEBLİ OLURUM?",
            typeId: "1",
            isNewPage: false,
            data: {

              "menuType1": {
                pageId: uuidv4()

              }
            }
          }, {
            id: uuidv4(),
            name: "NASIL HESAP AÇARIM?",
            typeId: "1",
            isNewPage: false,
            data: {
              "menuType1": {
                pageId: uuidv4()
              }
            }
          }
        ]
      }

    ]
  }, {
    id: uuidv4(),
    name: "Footer Menü",
    langId: 1,
    data: []
  }

];

var MenuApi = {
  getList: function() {

    return _list;

  }

};

export default MenuApi;
