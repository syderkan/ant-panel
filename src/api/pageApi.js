import _ from 'lodash';
import objectAssign from 'object-assign';

var _pageList = [];
const _treeData = [
  {
    label: 'Node1',
    value: '0-0',
    key: '0-0',
    children: [
      {
        label: 'Child Node1',
        value: '0-0-1',
        key: '0-0-1'
      }, {
        label: 'Child Node2',
        value: '0-0-2',
        key: '0-0-2'
      }
    ]
  }, {
    label: 'Node2',
    value: '0-1',
    key: '0-1'
  }
];

var PageApi = {
  getList: function() {

    return _pageList;

  },
  add: function(item) {

    _pageList.push(item);

  },
  getById: function(id) {

    var sourceItem = _.find(_pageList, {id: id});
    var pageItem = JSON.parse(JSON.stringify(sourceItem));
    return pageItem;

  },
  getTotalCount: function() {

    return _pageList.length;

  },
  edit: function(item) {
    var index = _.findIndex(_pageList, {id: item.id});
    _pageList.splice(index, 1, item);

  },
  getParentChildData: function() {

    return _treeData;

  }
};

export default PageApi;
