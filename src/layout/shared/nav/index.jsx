import React from 'react';
import {Layout, Menu, Icon} from 'antd';
const { Sider} = Layout;
import { Link, PropTypes } from 'react-router-dom'

class SharedNav extends React.Component {



  constructor(props, context) {
      super();

this.linkTo = this.linkTo.bind(this);
  }

  linkTo(item) {
console.log(item);
console.log(this.context);

  this.context.router.history.replace(item.key);
}

componentWillMount () {

    var pathname=this.getDefaultPathName(this.context.router);
    console.log(pathname);
    this.defaultSelectedKeys=[pathname];
}

getDefaultPathName(router){

let pathname=  router.route.location.pathname;

pathname = pathname.replace("/","");

if (pathname=='') {
  pathname="/";
}
return pathname;

}


    render() {

        return (

            <Sider trigger={null} collapsible collapsed={this.props.collapsed}>
                <div className="logo"/>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={this.defaultSelectedKeys} onClick={this.linkTo}>
                    <Menu.Item key="/">
                        <Icon type="user"/>
                        <span className="nav-text">Dashboard</span>
                    </Menu.Item>
                    <Menu.Item key="page">
                        <Icon type="video-camera"/>
                        <span className="nav-text">Page</span>
                    </Menu.Item>
                    <Menu.Item key="menu">
                        <Icon type="upload"/>
                        <span className="nav-text">Menu</span>
                    </Menu.Item>
                </Menu>
            </Sider>

        );

    }

}

SharedNav.contextTypes = {
 router: React.PropTypes.object
};

export default SharedNav;
