import React from 'react';
import {Layout, Icon} from 'antd';
const {Header, Content} = Layout;

class SharedHeader extends React.Component {

    constructor(props, context) {
        super();

        this.state = {
            collapsed: false
        };

        this.toggle = this.toggle.bind(this);

    }

    toggle() {
        this.setState({
            collapsed: !this.state.collapsed
        });

        this.props.onToggle(  !this.state.collapsed );
    }

    render() {

        return (

            <Header style={{
                background: '#fff',
                padding: 0,
                marginBottom:20
            }}>
                <Icon className="trigger" type={this.state.collapsed
                    ? 'menu-unfold'
                    : 'menu-fold'} onClick={this.toggle}/>
            </Header>
        );

    }

}

export default SharedHeader;
