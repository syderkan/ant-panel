var actionType = {
    "Page_Init": "Page_Init",
    "Page_Create":"Page_Create",
    "Menu_Init":"Menu_Init",
    "PageDropDown_Init":"PageDropDown_Init"
};

export default actionType;
