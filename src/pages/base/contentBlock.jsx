import React from 'react';
import uuidv1 from 'uuid/v1';
import {
  Card,
  Button,
  Row,
  Col,
  Icon,
  Modal
} from 'antd';
import ModuleList from './moduleList/index.jsx'
import {SortableContainer, SortableElement, arrayMove, SortableHandle} from 'react-sortable-hoc';

const DragHandle = SortableHandle(() => <Icon style={{
  cursor: "move"
}} type="bars"/>); // This can be any component you want

class ModuleItem extends React.Component
{
  constructor(props, context) {

    super(props);
  }

  render() {

    let ModuleContent = ModuleList[this.props.moduleItem.type];

    return (<ModuleContent key={this.props.moduleItem.id} value={this.props.moduleItem.data} style={{
      marginBottom: '10px'
    }}/>);

  }
}

const SortableItem = SortableElement(({value, removeModule}) => {

  return (

    <li style={{
      zIndex: 100000,
      listStyle: "none"
    }}>

      <Row gutter={16}>
        <Col span={2}>

          <DragHandle/>

        </Col>
        <Col span={16}>

          <ModuleItem moduleItem={value}/>

        </Col>

        <Col span={6}><Button onClick={removeModule.bind(this, value)} type="danger" icon="minus"/></Col>

      </Row>

    </li>
  );

});

const SortableList = SortableContainer(({items, removeModule}) => {
  return (
    <ul>
      {items.map((value, index) => (<SortableItem key={`item-${index}`} index={index} value={value} removeModule={removeModule}/>))}
    </ul>
  );
});

class ContentBlock extends React.Component {

  constructor(props, context) {

    super(props);
    this.addModule = this.addModule.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.removeModule = this.removeModule.bind(this);

    this.moduleSource = props.moduleSource;

    this.state = {
      moduleList: this.props.moduleList
    };

    this.onSortEnd = ({oldIndex, newIndex}) => {

      var changedList = arrayMove(this.state.moduleList, oldIndex, newIndex);
  this.props.onValueChange(this.props.blockId, changedList);
      this.setState({moduleList: changedList});
    };
  }

  addModule() {

    this.setState({addPopUpVisibility: true});

  }

  handleCancel() {

    this.setState({addPopUpVisibility: false});

  }

  selectModule(moduleKey, context)
  {
    var moduleList = this.state.moduleList;
    moduleList.push({id: uuidv1(), type: moduleKey, data: {}})

  this.props.onValueChange(this.props.blockId, moduleList);
    this.setState({moduleList: moduleList, addPopUpVisibility: false});

  }

  removeModule(moduleItem, context)
  {
    var moduleList = this.state.moduleList;

    _.remove(moduleList, function(module) {

      return module.id == moduleItem.id;

    });

    this.props.onValueChange(this.props.blockId, moduleList);

    this.setState({moduleList: moduleList});

  }

  render()
  {
    const moduleHtml = this.state.moduleList.map((moduleData, index) => <ModuleItem key={`module-${index}`} removeModule={this.removeModule} moduleItem={moduleData}/>);
    const gridStyle = {
      width: '25%',
      textAlign: 'center',
      cursor: 'pointer'
    };
    return (
      <Card title={this.props.title} noHovering={false} extra={< a href = "javascript:;" onClick = {
        this.addModule
      } > Yeni Ekle < /a>}>

        <Modal width="50%" title="Modül Ekleme" visible={this.state.addPopUpVisibility} footer={[ < Button key = "back" size = "large" onClick = {
            this.handleCancel
          } > İptal < /Button> ]} confirmLoading={this.state.confirmLoading}>

          <Card noHovering>
            {this.moduleSource.map((moduleItem, j) => <Card.Grid key={j} style={gridStyle} onClick={this.selectModule.bind(this, moduleItem.key)}>{moduleItem.text}</Card.Grid>)}

          </Card>

        </Modal>

        <SortableList items={this.state.moduleList} removeModule={this.removeModule} onSortEnd={this.onSortEnd} useDragHandle={true}/>

      </Card>
    )

  }

}

export default ContentBlock;
