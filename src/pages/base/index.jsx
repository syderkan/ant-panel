import React from 'react';
import {notification, Breadcrumb, Alert} from 'antd'
import uuidv1 from 'uuid/v1';

class BaseIndex extends React.Component {
  constructor(props, context) {
    super(props);
    this.addSuccess = this.addSuccess.bind(this);
    this.editCancel = this.editCancel.bind(this);
    this.openAddPopUp = this.openAddPopUp.bind(this);
    this.addCancel = this.addCancel.bind(this);
    this.openEditPopUp = this.openEditPopUp.bind(this);
    this.editFormRef = this.editFormRef.bind(this);
    this.change = this.change.bind(this);



    this.state = {
      dataSource: [],
      count: 0,
      isLoaded: false,
      confirmLoading: true,
      editItem: {}
    };
  }

  addCancel() {

    this.setState({addPopUpVisibility: false, editItem: this.store.getInitialItem(), error: undefined,editKey:uuidv1()});
    this.editForm.resetFields();

  }

  editCancel() {


    this.setState({editPopUpVisibility: false, editItem: this.store.getInitialItem(), error: undefined,editKey:uuidv1()});
    this.editForm.resetFields();
  }

  openAddPopUp() {

    this.setState({addPopUpVisibility: true});

  }

  addSuccess() {

    notification["success"]({message: 'Başarılı', description: 'Ekleme işlemi başarıyla tamamlandı.'});
  }
  editSuccess() {

    notification["success"]({message: 'Başarılı', description: 'Güncelleme işlemi başarıyla tamamlandı.'});
  }

  editFormRef(form) {

    this.editForm = form;
  }

  openEditPopUp(id) {

    this.setState({editPopUpVisibility: true, editItem: this.store.getById(id), editKey:uuidv1()});

  }

  initComponent()
  {
    if (!this.state.isLoaded) {

      this.action.init();
    }

  }

  componentWillMount() {

    this.store.addChangeListener(this.change)
    this.initComponent();
  }

  componentWillUnmount() {
    this.store.removeChangeListener(this.change)
  }

  change()
  {
    this.setState({"dataSource": this.store.getList(),editItem: this.store.getInitialItem() , "count": this.store.getTotalCount(), "isLoaded": true, "confirmLoading": false})

    if (this.store.getIsAddSuccess()) {

      this.setState({addPopUpVisibility: false, error: undefined});
      this.addSuccess();
      this.editForm.resetFields();
    }

    if (this.store.getIsEditSuccess()) {

      this.setState({editPopUpVisibility: false, error: undefined});
      this.editSuccess();
      this.editForm.resetFields();
    }
  }
}

const BaseBreadcrumb = function(props) {

  const itemList = props.data.map((breadcrumbItem) => <Breadcrumb.Item key={breadcrumbItem}>{breadcrumbItem}</Breadcrumb.Item>);

  return (
    <Breadcrumb className="breadcrumb">

      {itemList}

    </Breadcrumb>
  )
}

export {BaseBreadcrumb, BaseIndex};
