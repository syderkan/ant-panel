import React from 'react';
import {Alert} from 'antd'

class CustomAlert extends React.Component {

  constructor(props, context) {

    super(props);

    this.timeoutMiliSecond = 5000;

    this.state = {
      text: props.text,
      header: props.header
    };



  }

  s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }

  guid() {

    return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
  }

  getErrorMessage(errorData)
  {

    if (errorData) {

      let errorMessage = [];

      _.forOwn(errorData, function(errorItem, key) {

        _.forEach(errorItem.errors, function(fieldError) {
          errorMessage.push(fieldError.message);
        });

      });

      var errorHtml = errorMessage.map(function(messageItem, key) {
        return <div key={key}>{messageItem}</div>;
      });

      return errorHtml;

    }

    return null;

  }

  render() {

    var errorHtml = this.getErrorMessage(this.state.text);

    if (errorHtml) {
      return (<Alert key={this.guid()} message={this.state.header} description={errorHtml} type="error" showIcon closable/>)

    }

    return null;
  }

  componentWillReceiveProps(nextProps) {

   
    if (this.state.text != nextProps.text) {
      this.setState({header: nextProps.header, text: nextProps.text})

      setTimeout(function() {
        this.setState({text: undefined});
      }.bind(this), this.timeoutMiliSecond);
    }
  }

}

export default CustomAlert;
