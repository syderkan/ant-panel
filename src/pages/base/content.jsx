import React from 'react';
import _ from 'lodash'
import {Form, Input, Checkbox, Select} from 'antd';
import ContentBlock from './contentBlock.jsx'
import objectAssign from 'object-assign';

class Content extends React.Component {

  constructor(props, context) {

    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.getContentBlockHtml = this.getContentBlockHtml.bind(this);
    this.onValueChange = this.onValueChange.bind(this);
    this.getBlockList = this.getBlockList.bind(this);
    this.templateData = [
      {
        key: "mainPage",
        name: "Ana Sayfa",
        blockList: [
          {
            key: "mainContent",
            name: "Ana Kısım"
          }
        ]
      }
    ]

    this.moduleSource = [
      {
        key: "slider",
        text: "Slider"
      }, {
        key: "htmlContent",
        text: "Editor"
      }
    ]

    this.sourceData = this.props.value;
    // this.templateData = props.templateData;
    // this.moduleSource = props.moduleSource;

    this.optionList = _.map(this.templateData, function(item) {

      return {key: item.key, name: item.name};

    });

    var selectedTemplate = this.getSelectedTemplate(this.sourceData);

    var blockList = this.getBlockList(this.templateData, selectedTemplate);

    this.state = {
      blockList: blockList,
      selectedTemplate: selectedTemplate

    };

  }

  getBlockList(data, value)
  {
    var filterTemplateItem = _.filter(this.templateData, function(templateItem) {

      return templateItem.key == value;

    });

    if (filterTemplateItem) {
      return filterTemplateItem[0].blockList;
    }

    return [];

  }

  getSelectedTemplate(data) {

    var selectedTemplate = _.find(data, function(item) {
      return item.isSelected
    });

    if (selectedTemplate) {

      return selectedTemplate.key;
    }

    return null;

  }

  handleChange(value) {

    var blockList = this.getBlockList(this.templateData, value);
    this.setState({selectedTemplate: value, blockList: blockList});

  }

  getContentBlockHtml(blockItem, selectedTemplate)
  {

    var pageSource = _.find(this.sourceData, function(pageSource) {
      return pageSource.key == selectedTemplate;
    });

    var selectedBlock = _.find(pageSource.blockSource, function(blockSource) {

      return blockSource.blockKey == blockItem.key;

    });

    return (<ContentBlock moduleSource={this.moduleSource} onValueChange={this.onValueChange} key={blockItem.key} blockId={blockItem.key} title={blockItem.name} moduleList={selectedBlock.moduleList}/>);

  }

  onValueChange(blockId, moduleList) {

    var state = this.state;
    var templateData = _.find(this.sourceData, function(templateData) {
      return templateData.key == state.selectedTemplate
    });

    if (!templateData) {
      return;
    }

    var blockData = _.find(templateData.blockSource, function(blockItem) {
      return blockItem.blockKey == blockId;
    });

    console.log("blockItem changed");
    console.log(blockId);

    console.log("moduleList changed");
    console.log(moduleList);

    blockData.moduleList = moduleList;

    console.log("sourceData");
    console.log(this.sourceData);

    this.props.form.setFieldsValue({content: this.sourceData});
  }

  render()
  {
    const FormItem = Form.Item;
    const Option = Select.Option;

    const blockListHtml = this.state.blockList.map((item) => this.getContentBlockHtml(item, this.state.selectedTemplate));
    const optionListHtml = this.optionList.map((item) => <Option key={item.key} value={item.key}>{item.name}</Option>)

    return (
      <div>

        <FormItem label="Sayfa Tipi">

          <Select onChange={this.handleChange} value={this.state.selectedTemplate}>
            {optionListHtml}

          </Select>

        </FormItem>
        <FormItem >

          {blockListHtml}
        </FormItem>

      </div>
    );

  }

}
export default Content;
