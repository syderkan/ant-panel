import React from 'react';
import {Tag, Input, Tooltip, Button} from 'antd'

class EditableTagGroup extends React.Component {

  constructor(props, context) {

    super(props);

    this.handleClose = this.handleClose.bind(this);
    this.showInput = this.showInput.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleInputConfirm = this.handleInputConfirm.bind(this);
    this.saveInputRef = this.saveInputRef.bind(this);

    var value = [];

if (props.tags) {

  value=props.tags;
}

    this.state = {
      tags: value,
      inputVisible: false,
      inputValue: ''
    };
  }
  componentWillReceiveProps(nextProps) {
    // You don't have to do this check first, but it can help prevent an unneeded render
    if (nextProps.value !== this.state.tags) {

      if (nextProps.value) {
        this.setState({tags: nextProps.value});
      } else {
        this.setState({tags: []});
      }

    }
  }

  handleClose(removedTag) {
    const tags = this.state.tags.filter(tag => tag !== removedTag);
    console.log(tags);
    this.setState({tags});
  }

  showInput() {
    this.setState({
      inputVisible: true
    }, () => this.input.focus());
  }

  handleInputChange(e) {
    this.setState({inputValue: e.target.value});
  }

  handleInputConfirm() {
    const state = this.state;
    const inputValue = state.inputValue;
    let tags = state.tags;
    if (inputValue && tags.indexOf(inputValue) === -1) {
      tags = [
        ...tags,
        inputValue
      ];
    }

    this.props.onChange(tags);
    this.setState({tags, inputVisible: false, inputValue: ''});
  }

  saveInputRef(input) {
    this.input = input
  }

  render() {
    const {tags, inputVisible, inputValue} = this.state;
    return (
      <div>

        {tags.map((tag, index) => {
          const isLongTag = tag.length > 20;
          const tagElem = (
            <Tag key={tag} closable={true} afterClose={() => this.handleClose(tag)}>
              {isLongTag
                ? `${tag.slice(0, 20)}...`
                : tag}
            </Tag>
          );
          return isLongTag
            ? <Tooltip title={tag}>{tagElem}</Tooltip>
            : tagElem;
        })}
        {inputVisible && (<Input ref={this.saveInputRef} type="text" size="small" style={{
          width: 78
        }} value={inputValue} onChange={this.handleInputChange} onBlur={this.handleInputConfirm} onPressEnter={this.handleInputConfirm}/>)}
        {!inputVisible && <Button size="small" type="dashed" onClick={this.showInput}>+ Yeni</Button>}

      </div>

    );
  }
}

export default EditableTagGroup;
