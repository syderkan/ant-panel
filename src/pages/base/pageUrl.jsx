import React from 'react';
import {Form, Input, Checkbox} from 'antd';

class PageUrl extends React.Component {

  constructor(props, context)
  {
    super(props);

    this.isCustomUrlChange = this.isCustomUrlChange.bind(this);
    this.handleChange = this.handleChange.bind(this);

    let isCustomUrl = false;
    if (props.isCustomUrl) {
      isCustomUrl = props.isCustomUrl;
    }


    this.state = {
      name: props.name,
      url: props.url,
      isCustomUrl: isCustomUrl
    };

    const {getFieldValue, setFieldsValue, getFieldDecorator} = this.props.form;
    this.getFieldDecorator = getFieldDecorator;
    this.setFieldsValue = setFieldsValue;
    this.getFieldValue = getFieldValue;





  }

  isCustomUrlChange() {

console.log("isCustomUrlChange");

    if (this.state.isCustomUrl) {

      var name = this.getFieldValue('name')
      let value = this.getFilterUrl(name);

      console.log(value);

      this.setFieldsValue({url: value})
    }

this.setFieldsValue({
  isCustomUrl: !this.state.isCustomUrl
});

    this.setState({
      isCustomUrl: !this.state.isCustomUrl
    });

  }

  getFilterUrl(url)
  {

    return url;
  }

  handleChange(event) {

    if (!this.state.isCustomUrl) {

      let value = this.getFilterUrl(event.target.value);
      this.setFieldsValue({url: value})
    }

  }



  render() {

    const FormItem = Form.Item;

    return (
      <div>
        <FormItem label="Sayfa Adı">
          {this.getFieldDecorator('name', {
            initialValue: this.props.name,
            rules: [
              {
                required: true,
                message: 'Sayfa adı giriniz.'
              }
            ]

          })(<Input type="text" onChange={this.handleChange}/>)}
        </FormItem>

        <FormItem label="Sayfa Adresi" style={{marginBottom:'0px'}}>
          {this.getFieldDecorator('url', {
            initialValue: this.props.url,
            rules: [
              {
                required: true,
                message: 'Sayfa adresini giriniz.'
              }
            ]

          })(<Input style={{width:"100%"}} addonBefore="//" type="text" disabled={!this.state.isCustomUrl}/>)
}





        </FormItem>
        <FormItem>
        {this.getFieldDecorator('isCustomUrl', {
          initialValue: this.props.isCustomUrl,


        })(<Checkbox onChange={this.isCustomUrlChange} checked={this.state.isCustomUrl}>Adresi Elle Güncelle
        </Checkbox>)}
        </FormItem>
      </div>
    )
  }

}

export default PageUrl;
