import React from 'react';
import StatuTag from './statuTag.jsx'

var columns =function (editHandler)
{

return    [
         {
             title: 'Ad',
             dataIndex: 'name',
             key: 'name'
         },   {
             title: 'Durum',
             dataIndex: 'statu',
             key: 'statu',
             render:(text,record)=>(

 StatuTag(record.statuId)

             )
         }, {
             title: 'İşlem',
             key: 'action',
             render: (text, record) => (
                 <span>
                     <a onClick={() => editHandler(record.id)}>Düzenle</a>
                     <span className="ant-divider"/>
                     <a href="#">Sil</a>
                 </span>
             )
         }
     ];


}

export default columns;
