import React from 'react';
import EditForm from './editForm.jsx'
import {
  Breadcrumb,
  Row,
  Col,
  Button,
  Table,
  Icon,
  Modal,
  Alert
} from 'antd'

import {BaseIndex, BaseBreadcrumb} from '../base/index.jsx';
import CustomAlert from '../base/customAlert.jsx'
import store from '../../stores/pageStore';
import action from '../../actions/pageAction';
import columns from './columns';
import _ from 'lodash'
import assign from 'object-assign';

class PagePage extends BaseIndex {

  constructor(props, context) {
    super(props);

    this.addOk = this.addOk.bind(this);
    this.editOk = this.editOk.bind(this);
    this.store = store;
    this.action = action;

  }

  addOk() {

    const form = this.editForm;

    form.validateFields((err, values) => {
      if (err) {

        var generateError = assign({}, err);
        this.setState({error: generateError, errorHeader: 'Doğrulama'});
        return;
      }

      this.setState({"confirmLoading": true});
      this.action.create(values);

    });

  }

  editOk() {

    const form = this.editForm;

    form.validateFields((err, values) => {
      if (err) {

        var generateError = assign({}, err);
        this.setState({error: generateError, errorHeader: 'Doğrulama'});
        return;
      }

      this.setState({"confirmLoading": true});
      this.action.edit(values);

    });

  }

  render() {

    return (

      <div>

        <BaseBreadcrumb data={['Dashboard', 'Sayfa']}/>
        <div className="main-content">

          <div className="inner-content">
            <Row className="mb20">

              <Col offset={12} span={12} style={{
                'textAlign': 'right'
              }}>

                <Button icon="plus" onClick={this.openAddPopUp}>
                  Yeni Ekle</Button>
              </Col>
            </Row>

            <Row>
              <Col span={24}>
                <Table locale={{
                  emptyText: "Kayıt bulunamadı"
                }} columns={columns(this.openEditPopUp)} dataSource={this.state.dataSource} rowKey="id"/>
              </Col>
            </Row>
          </div>

          <Modal width="50%" title="Sayfa Ekleme" visible={this.state.addPopUpVisibility} onCancel={this.addCancel} confirmLoading={this.state.confirmLoading} footer={[ < Button key = "back" onClick = {
              this.addCancel
            } > İptal < /Button>,<Button key="save" type="dashed" onClick={this.addOk}> Kaydet </Button >, < Button key = "publish" type = "primary" onClick = {
              this.addOk
            } > Yayınla < /Button> ]}>

            <CustomAlert header={this.state.errorHeader} text={this.state.error}/>

            <EditForm ref={this.editFormRef} editItem={this.state.editItem} key={this.state.editKey}/>
          </Modal>

          <Modal title="Sayfa Güncelleme" visible={this.state.editPopUpVisibility} onCancel={this.editCancel} confirmLoading={this.state.confirmLoading} footer={[ < Button key = "back" onClick = {
              this.editCancel
            } > İptal < /Button>, <Button key="save" type="dashed" onClick={this.editOk} > Kaydet </Button >, < Button key = "publish" type = "primary" > Yayınla < /Button> ]}>
            <EditForm ref={this.editFormRef} editItem={this.state.editItem} key={this.state.editKey}/>
          </Modal>

        </div>
      </div>
    );

  }

}

export default PagePage;
