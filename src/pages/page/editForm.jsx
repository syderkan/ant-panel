import React from 'react';
import {
  Form,
  Icon,
  Input,
  Button,
  Checkbox,
  Tabs,
  Row,
  Col,
  Select,
  TreeSelect
} from 'antd';
import CustomTag from '../base/tag.jsx';
import PageUrl from '../base/pageUrl.jsx';
import Content from '../base/content.jsx';
import PageDropDown from '../PageDropDown/PageDropDown.jsx';

const TabPane = Tabs.TabPane;
const FormItem = Form.Item;

class EditForm extends React.Component {

  constructor(props, context) {

    super(props);
    this.tagsChange = this.tagsChange.bind(this);
    this.parentIdChange = this.parentIdChange.bind(this);

  }

  tagsChange(data) {

    console.log(data);
    this.props.form.setFieldsValue({seoKeyword: data});
  }

  parentIdChange(value,label)
  {
this.props.form.setFieldsValue({"parentId":value } );
  }

  render() {



    const {getFieldDecorator} = this.props.form;

    return (
      <Form className="login-form">

      {getFieldDecorator('id', {

        initialValue: this.props.editItem.id

      })(

<input type="hidden"   />

      )
}


        <Row gutter={16}>
          <Col className="gutter-row" span={16}>
            <Tabs defaultActiveKey="1">
              <TabPane tab="Temel" key="1">

                <PageUrl name={this.props.editItem.name} url={this.props.editItem.url} isCustomUrl={this.props.editItem.isCustomUrl} form={this.props.form}/>

              </TabPane>
              <TabPane tab="İçerik" key="2">

                <FormItem label="Dil">

                  {getFieldDecorator('langId', {

                    initialValue: this.props.editItem.langId,
                    valuePropName: "defaultValue"
                  })(

                    <Select >
                      <Select.Option value="1">Türkçe</Select.Option>
                      <Select.Option value="2">İngilizce</Select.Option>

                    </Select>

                  )
}

                </FormItem>

                {getFieldDecorator('content', {initialValue: this.props.editItem.content})(<Content form={this.props.form}/>)
}

              </TabPane>
              <TabPane tab="Seo" key="3">

                <FormItem label="Seo Başlık">

                  {getFieldDecorator('seoTitle', {initialValue: this.props.editItem.seoTitle})(<Input type="text"/>)}

                </FormItem>

                <FormItem label="Seo Açıklama">

                  {getFieldDecorator('seoDescription', {initialValue: this.props.editItem.seoDescription})(<Input type="textarea"/>)}

                </FormItem>

                <FormItem label="Seo Anahtar Kelime">

                  {getFieldDecorator('seoKeyword', {initialValue: this.props.editItem.seoKeyword})(<CustomTag tags={this.props.editItem.seoKeyword} onChange={this.tagsChange}/>)}

                </FormItem>

              </TabPane>

            </Tabs>

          </Col>
          <Col className="gutter-row" span={8}>

            <Tabs defaultActiveKey="1">
              <TabPane tab="Genel" key="1">

                <FormItem label="Durum">

                  {getFieldDecorator('statuId', {initialValue: this.props.editItem.statuId})(

                    <Select >
                      <Select.Option value="1">Taslak</Select.Option>
                      <Select.Option value="2">Yayında</Select.Option>
                      <Select.Option value="3" disabled>Yayında(Düzenleme)</Select.Option>

                    </Select>

                  )
}

                </FormItem>

                <FormItem label="Sayfa Yeri">

                  {getFieldDecorator('parentId', {initialValue: this.props.editItem.parentId})(

<PageDropDown onChange={ this.parentIdChange } />


                )
}

                </FormItem>

              </TabPane>

            </Tabs>

          </Col>

        </Row>

      </Form>
    );
  }

}

const EditFormWithValidation = Form.create({})(EditForm);

export default EditFormWithValidation;
