import React from 'react'
import {Form, Input} from 'antd';

import PageDropDown from '../PageDropDown/PageDropDown.jsx';

const FormItem = Form.Item;

class MenuType1 extends React.Component {

  constructor(props, context) {

    super(props);

    console.log(props);

    var propsData = {
      pageId: ""
    };

    if (this.props.data) {
      propsData = this.props.data;
    }

    this.state = {

      pageId: propsData.pageId
    }

    this.pageChange = this.pageChange.bind(this);
  }



  isValid(success)
  {
    this.props.form.validateFields((err, values) => {

      if (err) {

        return false;
      }

      success(values);

    });

  }

pageChange(value,label)
{
console.log("pageIdChange" + value)
this.props.form.setFieldsValue({"pageId":value } );

}

  render() {

    const formItemLayout = this.props.layout;
    const {getFieldDecorator} = this.props.form;

    return (
      <div>

        <FormItem label="Sayfa" {...formItemLayout}>
          {getFieldDecorator('pageId', {
            initialValue: this.state.pageId,
            rules: [
              {
                required: true,
                message: 'Sayfa Seçiniz.'
              }
            ]

          })( <PageDropDown onChange={ this.pageChange } />)}
        </FormItem>

      </div>
    );

  }
}

const MenuType1WithValidation = Form.create()(MenuType1);

export default MenuType1WithValidation;
