import React from 'react'
import {Form, Input} from 'antd';
const FormItem = Form.Item;

class MenuType2 extends React.Component {

  constructor(props, context) {

    super(props);

    var propsData = {
      url: ""
    };

    if (this.props.data) {

      propsData = this.props.data;
    }

    this.state = {

      url: propsData.url

    }

     
    this.isValid = this.isValid.bind(this);

  }



  isValid(success)
  {
    this.props.form.validateFields((err, values) => {

      if (err) {

        return false;
      }

      success(values);

    });

  }

  render() {

    const formItemLayout = this.props.layout;
    const {getFieldDecorator} = this.props.form;

    return (
      <div>

        <FormItem label="Url" {...formItemLayout}>
          {getFieldDecorator('url', {
            initialValue: this.state.url,
            rules: [
              {
                required: true,
                message: 'Sayfa adresi giriniz.'
              }
            ]

          })(<Input/>)}
        </FormItem>

      </div>
    );

  }
}

const MenuType2WithValidation = Form.create()(MenuType2);

export default MenuType2WithValidation;
