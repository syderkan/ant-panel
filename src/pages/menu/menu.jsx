import React from 'react'
import {Collapse} from 'antd';

import store from '../../stores/menuStore';
import action from '../../actions/menuAction';
import MenuContent from './menuContent.jsx'

const Panel = Collapse.Panel;

import {BaseBreadcrumb} from '../base/index.jsx';

class MenuPage extends React.Component {

  constructor(props, context)
  {
    super(props);

    this.change = this.change.bind(this);

    this.state = {
      dataSource: []
    };

  }

  initComponent()
  {
    if (!this.state.isLoadded) {

      action.init();
    }

  }

  componentWillMount() {

    store.addChangeListener(this.change)
    this.initComponent();
  }

  componentWillUnmount() {
    store.removeChangeListener(this.change)
  }

  change()
  {
    this.setState({"dataSource": store.getList(), "confirmLoading": false})

  }

  render() {

    const customPanelStyle = {
      background: '#f7f7f7',
      borderRadius: 4,
      marginBottom: 24,
      border: 0
    };

    const listItems = this.state.dataSource.map((item, index) => <Panel header={item.name} key={index} style={customPanelStyle}><MenuContent data={item.data} /></Panel>);

    return (
      <div>

        <BaseBreadcrumb data={['Dashboard', 'Menu']}/>
        <div className="main-content">

          <div className="inner-content">

            <Collapse bordered={false} defaultActiveKey={['-1']}>
              {listItems}

            </Collapse>

          </div>
        </div>
      </div>
    )

  }

}

export default MenuPage;
