import React from 'react'
import {Form, Select, Input, Checkbox, Switch} from 'antd';
import MenuType1 from './menuType1.jsx'
import MenuType2 from './menuType2.jsx'

const FormItem = Form.Item;

class EditForm extends React.Component {

  constructor(props, context) {

    super(props, context);

    console.log("EditForm");
    console.log(this.props);

    this.state = {

      typeId: this.props.nodeData.typeId,
      data: this.props.nodeData.data,
      name: this.props.nodeData.name,
      id: this.props.nodeData.id,
      isNewPage: this.props.nodeData.isNewPage
    }

    this.isValid = this.isValid.bind(this);
    this.getMenuForm = this.getMenuForm.bind(this);
    this.handleTypeIdChange = this.handleTypeIdChange.bind(this);
    this.isNewPageChange = this.isNewPageChange.bind(this);
  }

  isNewPageChange(checked)
  {

    this.props.form.setFieldsValue({isNewPage: checked});
  }

  handleTypeIdChange(e)
  {
    console.log(e);
    this.setState({typeId: e});
  }

  getMenuForm(typeId, data, layout)
  {
    if (typeId == 1) {

      return (<MenuType1 layout={layout} data={this.state.data.menuType1} wrappedComponentRef={(inst) => this.menuType1 = inst} />);
    }

    if (typeId == 2) {
      return (<MenuType2 layout={layout} data={this.state.data.menuType2} wrappedComponentRef={(inst) => this.menuType2 = inst}/>);
    }

  }

  isValid(success)
  {
    var name = this.props.form.getFieldValue("name");
    var typeId = this.state.typeId;
    var id = this.state.id;
    var isNewPage = this.props.form.getFieldValue("isNewPage");

    if (typeId == 1) {

      this.menuType1.isValid(function(data) {

        var menuData = {
          menuType1: data
        };

        success(id, name, menuData, typeId, isNewPage);

      });
    }

    if (typeId == 2) {

      this.menuType2.isValid(function(data) {

        var menuData = {
          menuType2: data
        };

        success(id, name, menuData, typeId, isNewPage);

      });
    }

  }

  render()
  {

    const {getFieldDecorator} = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: {
          span: 24
        },
        sm: {
          span: 8
        }
      },
      wrapperCol: {
        xs: {
          span: 24
        },
        sm: {
          span: 14
        }
      }
    };
    const menuContent = this.getMenuForm(this.state.typeId, this.state.data, formItemLayout);

    return (
      <Form>

        <FormItem label="Başlık" {...formItemLayout}>
          {getFieldDecorator('name', {initialValue: this.state.name})(<Input/>)}
        </FormItem>

        <FormItem label="Yeni Pencerede Aç" {...formItemLayout}>

          {getFieldDecorator('isNewPage', {
            initialValue: this.state.isNewPage,
            valuePropName: "defaultChecked"
          })(<Switch onChange={this.isNewPageChange}/>)
}

        </FormItem>

        <FormItem label="Menü Tipi" {...formItemLayout}>

          {getFieldDecorator('typeId', {initialValue: this.state.typeId})(

            <Select onChange={this.handleTypeIdChange}>
              <Select.Option value="1">İçerik Sayfa</Select.Option>
              <Select.Option value="2">Özel</Select.Option>

            </Select>

          )
}

        </FormItem>

        {menuContent}

      </Form>
    );

  }

}

const EditFormWithValidation = Form.create({})(EditForm);

export default EditFormWithValidation;
