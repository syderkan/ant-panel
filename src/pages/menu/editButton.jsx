import React from 'react'
import {Button, Modal} from 'antd';
import EditForm from './editForm.jsx';
import uuidv4 from 'uuid/v4';

class EditButton extends React.Component {

  constructor(props, context)
  {
    super(props);

    this.state = {
      nodeData:props.nodeData
    };

    this.handleCancel = (e) => {

      this.setState({visible: false});
    }

    this.editClick = (e) => {

      this.setState({visible: true, editKey: uuidv4()});
    }

    this.handleOk = this.handleOk.bind(this);
  }

  handleOk()
  {

    var onSuccess = (id, name, data, typeId,isNewPage) => {


      this.setState({visible: false, data: data, typeId: typeId});
      this.props.onChange({data: data, typeId: typeId, name: name, id: id, isNewPage:isNewPage});
    }

    this.editForm.isValid(onSuccess);

  }

  render()
  {
    return (
      <div>
        <Button icon="edit" onClick={this.editClick} style={{
          marginRight: "10px"
        }}/>

        <Modal okText="Tamam" cancelText="İptal" title="Düzenle" visible={this.state.visible} onOk={this.handleOk} onCancel={this.handleCancel}>
          <EditForm wrappedComponentRef={(inst) => this.editForm = inst} key={this.state.editKey}  nodeData={this.state.nodeData}/>
        </Modal>

      </div>
    );

  }

}

export default EditButton;
