import React from 'react'
import SortableTree, {changeNodeAtPath} from 'react-sortable-tree';
import assign from 'object-assign';
import {Button, Icon} from 'antd';
import EditButton from './editButton.jsx';
import _ from 'lodash';
import uuidv4 from 'uuid/v4';

class MenuContent extends React.Component {

  constructor(props) {

    super(props);

    this.onTreeChange = this.onTreeChange.bind(this);
    this.nodeChange = this.nodeChange.bind(this);
    this.nodeDataChange = this.nodeDataChange.bind(this);

    this.state = {
      data: this.props.data
    }

  }

  onTreeChange(treeData)
  {
    this.setState({data: treeData});
  }

  generateNode(node, path, nodeChange, getNodeKey)
  {
    return {
      "title": node.name,
      "buttons": [ < EditButton key = {
          uuidv4()
        }
        
        nodeData={ node}

        onChange = {
          this.nodeDataChange
        } />, < Button type = "danger" icon = "minus" />
      ]
    }

  }

  nodeDataChange(data)
  {
    console.log("node content change");
    console.log(data);
    console.log(this.state.data);
    var selectedNode = _.find(this.state.data, function(o) {
      return o.id == data.id;
    });

    if (selectedNode) {

      selectedNode = assign(selectedNode, data);

      this.setState({treeData: this.state.data});
    }

  }

  nodeChange(event, path, node, getNodeKey)
  {
    const name = event.target.value;
    node.name = name;
    this.setState(state => ({
      treeData: changeNodeAtPath({treeData: state.data, path: path, getNodeKey: getNodeKey, newNode: node})
    }));

  }

  render() {

    const getNodeKey = ({treeIndex}) => treeIndex;

    return (

      <div>
        {this.state.data.name}

        <div>
          <div style={{
            height: 300
          }}>
            <SortableTree treeData={this.state.data} onChange={this.onTreeChange} generateNodeProps={({node, path}) => {
              return this.generateNode(node, path, this.nodeChange, getNodeKey);
            }}/>
          </div>
        </div>

        <div>
          <pre>{JSON.stringify(this.state.data, null, 2) }</pre>
        </div>;
      </div>

    );
  }

}

export default MenuContent;
