import React from 'react'
 
import {BaseBreadcrumb} from '../base/index.jsx';

const DashboardPage = (props) => {

    return (

        <div>
<BaseBreadcrumb data={['Dashboard']}/>
            <div className="main-content">

                <div className="inner-content">

                    Dashboard</div>
            </div>
        </div>
    )

}

export default DashboardPage;
