import React from 'react'

import {TreeSelect} from 'antd';

import store from '../../stores/pageDropDownStore';
import action from '../../actions/pageDropDownAction';

class PageDropDown extends React.Component {

  constructor(props, context)
  {
    super(props);

    this.state = {
      dataSource: [],
      isLoaded: false,
      value: this.props.value
    };

    this.change = this.change.bind(this);
    this.onChange = this.onChange.bind(this);

  }

  componentWillMount() {

    store.addChangeListener(this.change)
    this.initComponent();
  }

  componentWillUnmount() {
    store.removeChangeListener(this.change)
  }

  initComponent()
  {
    if (!this.state.isLoadded) {

       action.init();
    }

  }

  change()
  {
    this.setState({"dataSource": store.getList(), "confirmLoading": false});
  }

  onChange(value,label)
  {
    console.log(value);
    this.setState({value: value});
this.props.onChange(value,label);
  }

  render() {
    return (<TreeSelect style={{
      width: '100%'
    }} value={this.state.value} dropdownStyle={{
      maxHeight: 400,
      overflow: 'auto'
    }}  treeData={this.state.dataSource} placeholder="Please select" treeDefaultExpandAll onChange={this.onChange}/>);
  }

}

export default PageDropDown;
