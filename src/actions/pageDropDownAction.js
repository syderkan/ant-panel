import Dispatcher from '../dispatcher/appDispatcher';
import ActionTypes from '../constant/actionType';

var PageDropDownAction = {

    "init": function() {
        Dispatcher.dispatch({actionType: ActionTypes.PageDropDown_Init});
    }
    
};

export default PageDropDownAction;
