import Dispatcher from '../dispatcher/appDispatcher';
import ActionTypes from '../constant/actionType';

var PageAction = {
    "init": function() {
        Dispatcher.dispatch({actionType: ActionTypes.Page_Init});
    },
    "create": function(item) {
        Dispatcher.dispatch({actionType: ActionTypes.Page_Create, payload: item});
    },
    "edit":function(item)
    {
Dispatcher.dispatch({actionType: ActionTypes.Page_Edit, payload: item});

    }

};

export default PageAction;
