import Dispatcher from '../dispatcher/appDispatcher';
import ActionTypes from '../constant/actionType';

var MenuAction = {
    "init": function() {
        Dispatcher.dispatch({actionType: ActionTypes.Menu_Init});
    }

};

export default MenuAction;
