import React from 'react';
import {render} from 'react-dom';
import {Layout} from 'antd';
const {Content} = Layout;
import SharedNav from './layout/shared/nav/index.jsx';
import SharedHeader from './layout/shared/header/index.jsx';
import {HashRouter as Router, Route} from 'react-router-dom'
import Dashboard from './pages/dashboard/dashboard.jsx'
import Page from './pages/page/index.jsx'
import Menu from './pages/menu/menu.jsx'
import './css/main.css';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            menuCollapse: false
        };

        this.menuToggle = this.menuToggle.bind(this)
    }

    menuToggle(isCollapse) {

        this.setState({
            menuCollapse: isCollapse
          });
    }

    render() {
        return (
          <Router>
            <Layout className={'ant-layout-has-sider'}>
                <SharedNav collapsed={this.state.menuCollapse}/>
                <Layout>
                    <SharedHeader onToggle={this.menuToggle}/>
                    <Content>
                      <Route exact path="/" component={Dashboard}/>
                      <Route exact path="/page" component={Page}/>
                          <Route exact path="/menu" component={Menu}/>
                    </Content>
                </Layout>
            </Layout>
            </Router>
        );
    }
};

render(
    <App/>, document.getElementById('app'));
