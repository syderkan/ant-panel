import Dispatcher from '../dispatcher/appDispatcher';
import ActionTypes from '../constant/actionType';
import Events from 'events';
import assign from 'object-assign';
import uuidv1 from 'uuid/v1';
import MenuApi from '../api/menuApi';

var CHANGE_EVENT = 'change';

var MenuStore = assign({}, Events.EventEmitter.prototype, {
  list: [],
  addChangeListener: function(callback) {

    this.on(CHANGE_EVENT, callback);
  },
  removeChangeListener: function(callback) {

    this.removeListener(CHANGE_EVENT, callback);
  },
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },
  getList: function() {
    return this.list;
  },
  loadList: function(onSuccess) {
    this.list = MenuApi.getList();

    if (onSuccess) {
      onSuccess();
    }

  }

});

Dispatcher.register(function(action) {



  switch (action.actionType) {

    case ActionTypes.Menu_Init:

      MenuStore.loadList(function () {

        MenuStore.emitChange();
      });


      break;

    default:
      // no op
  }
});

export default MenuStore;
