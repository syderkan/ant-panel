import Dispatcher from '../dispatcher/appDispatcher';
import ActionTypes from '../constant/actionType';
import Events from 'events';
import PageApi from '../api/pageApi';
import assign from 'object-assign';
import uuidv1 from 'uuid/v1';

var CHANGE_EVENT = 'change';

var _total = 0;
var _isAddSuccess = false;
var _isEditSuccess = false; 

var PageStore = assign({}, Events.EventEmitter.prototype, {

  addChangeListener: function(callback) {

    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {

    this.removeListener(CHANGE_EVENT, callback);
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },
  getList: function() {
    return PageApi.getList();
  },
  getTotalCount: function() {
    return PageApi.getTotalCount();

  },
  getIsAddSuccess: function() {

    return _isAddSuccess;
  },
  getIsEditSuccess: function() {
    return _isEditSuccess;
  },
  getById: function(id) {

    return PageApi.getById(id);

  },
  getInitialItem: function() {

    var initialContent = [
      {
        key: "mainPage",
        isSelected: true,
        blockSource: [
          {
            "blockKey": "mainContent",
            moduleList: [
              {
                id: "2",
                type: "htmlContent",
                data: {
                  content: "<p>test</p>"
                }
              }, {
                id: "3",
                type: "htmlContent",
                data: {
                  content: "<p>test2</p>"
                }
              }
            ]
          }

        ]
      }
    ];

    return assign({}, {
      id: uuidv1(),
      langId: "1",
      parentId: "root",
      statuId: "1",
      content: initialContent
    });

  }

});

Dispatcher.register(function(action) {

  _isAddSuccess = false;
  _isEditSuccess = false;

  switch (action.actionType) {

    case ActionTypes.Page_Init:

      _total = 3;
      PageStore.emitChange();

      break;

    case ActionTypes.Page_Create:

      action.payload.id = uuidv1();
      PageApi.add(action.payload);
      _isAddSuccess = true;
      PageStore.emitChange();

      break;

    case ActionTypes.Page_Edit:
      PageApi.edit(action.payload);
      _isEditSuccess = true;
      PageStore.emitChange();

      break;

    default:
      // no op
  }
});

export default PageStore;
