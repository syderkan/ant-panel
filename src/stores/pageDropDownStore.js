import Dispatcher from '../dispatcher/appDispatcher';
import ActionTypes from '../constant/actionType';
import Events from 'events';
import assign from 'object-assign';
import uuidv1 from 'uuid/v1';
import PageApi from '../api/pageApi';

var CHANGE_EVENT = 'change';

var PageDropDownStore = assign({}, Events.EventEmitter.prototype, {

  addChangeListener: function(callback) {

    this.on(CHANGE_EVENT, callback);
  },
  removeChangeListener: function(callback) {

    this.removeListener(CHANGE_EVENT, callback);
  },
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },
  getList: function() {
    return PageApi.getParentChildData();
  }

});

Dispatcher.register(function(action) {

  switch (action.actionType) {

    case ActionTypes.PageDropDown_Init:

        PageDropDownStore.emitChange();

      break;

    default:
      // no op
  }
});

export default PageDropDownStore;
